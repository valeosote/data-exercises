# ttc8030-harjoitukset

My exercises for the Jamk course "Datan esikäsittely".

The original repository for the exercises in this folder can be found here:
[https://github.com/M11k4/JAMK-AIDA-dataprep-course](https://github.com/M11k4/JAMK-AIDA-dataprep-course).

In this course I really enjoyed the harjoitustyö, which brought together most of the material in the course. You can find it here: [harkkatyö](https://gitlab.com/valeosote/data-exercises/-/blob/main/JAMK-AIDA-dataprep-course/harjoitustyo/harjoitustyo.ipynb)