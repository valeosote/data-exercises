# TTC8010-harjoitukset

My exercises for the Jamk course "Laskennalliset Algoritmit".

In this course I really enjoyed figuring out my own recursive functions, where I went above and beyond what was required. I also got in some good reps using the basic machine learning methods.