The original repository for the exercises in this folder can be found here:
[https://github.com/M11k4/JAMK-AIDA-intro-course](https://github.com/M11k4/JAMK-AIDA-intro-course).

In this course I really enjoyed the data visualizations, such as the graphs for K-means clustering and KNN algorithms.
